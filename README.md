# Solaxcloud download

Command line tool to download site data from solaxcloud.com as json files
per day.

## Installation

Install GNU bash, coreutils and bc.

Clone or download the repository.

## Usage

```bash
bash ./slx_dl.sh -l
slx_dl.sh [ --site_id SITE_ID ] [ --start_date START_DATE ] \
        [ --rsleep RSLEEP ] [ --longhelp ] [ --end_date END_DATE ] [ --token TOKEN ] \
        [ --help ]

 -i | --site_id   Site ID from solaxcloud.com.
 -s | --start_date
                  Start date of the range to download data from. YYYY-MM-DD format.
 -p | --rsleep    Sleep for approximately number of seconds between data requests to solaxcloud.com. Defaults to approx 3s.
                  Default: 3
 -l | --longhelp  Show long help, incl. where to find site_id and security token. Do nothing else.
 -e | --end_date  End date of the range to download data from. YYYY-MM-DD format. Optional. Only download for start date is end date is omitted.
 -t | --token     Security token from solaxcloud.com.
 -h | --help      Show this help message
```

To download data from solaxcloud.com you need your site ID and an authoriation
token. Obtain them by logging in to solaxcloud.com and using your browsers dev
tools for network analysis:

1. Surf and authenticate to solaxcloud.com.

2. Open site analysis.

3. Open your browsers developer tools, network tab.

4. Request data using one of the date controls.

5. In the request headers, search for "token". This is the authorization token
for your current session.

6. In the request data search for "siteid". This is your site id.

## Credits

<https://github.com/Anvil/bash-argsparse>
