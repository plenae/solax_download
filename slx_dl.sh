#!/bin/bash
#
# Download data from solaxcloud.

PATH=$PATH:./lib:.

source argsparse.sh
source dateutil.sh

argsparse_use_option start_date "Start date of the range to download data from. YYYY-MM-DD format." short:s value type:date
argsparse_use_option end_date "End date of the range to download data from. YYYY-MM-DD format. Optional. Only download for start date is end date is omitted." short:e value type:date

argsparse_use_option token "Security token from solaxcloud.com." short:t value
argsparse_use_option site_id "Site ID from solaxcloud.com." short:i value

argsparse_use_option rsleep "Sleep for approximately number of seconds between data requests to solaxcloud.com. Defaults to approx 3s." short:p value type:uint default:3

argsparse_use_option longhelp "Show long help, incl. where to find site_id and security token. Do nothing else." short:l

function show_long_help(){
    argsparse_usage
    cat << EOF

To download data from solaxcloud.com you need your site ID and an authoriation token. Obtain them by logging in to solaxcloud.com and using your browsers dev tools for network analysis:
1. Surf and authenticate to solaxcloud.com.
2. Open site analysis.
3. Open your browsers developer tools, network tab.
4. Request data using one of the date controls.
5. In the request headers, search for "token". This is the authorization token for your current session.
6. In the request data search for "siteid". This is your site id.
EOF
	exit 0
}

# Requires global vars start_date, end_date, rsleep, site_id, token
function download_slx_data(){
    # Loop over days between $start_date and $end_date and request data using curl.
    # https://stackoverflow.com/a/28226339
    d=$start_date
    day_after_end_date=$(date -I -d "${end_date} + 1 day")
    while [ "$d" != "$day_after_end_date" ]; do 
        OUTFILE=./solax_${site_id}_${d}.json

        # Random value to wait between $rsleep/2.00 and $rsleep*1.50
        if [ "$d" == "$end_date" ]
        then
            timeout=0
        else
            rand_val=$((RANDOM%($rsleep*100)))
            timeout=$(echo "scale=2; $rsleep/2 + $rand_val/100" | bc)
        fi
        echo Downloading $d

        curl -s 'https://www.solaxcloud.com/phoebus/site/getSiteTotalPower' --compressed -X POST -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/114.0' -H 'Accept: application/json, text/plain, */*' -H 'Accept-Language: nl-BE,nl;q=0.8,en-US;q=0.5,en;q=0.3' -H 'Accept-Encoding: gzip, deflate, br' -H 'Content-Type: application/x-www-form-urlencoded;charset=UTF-8' -H "token: ${token}" -H 'lang: nl_NL' -H 'Origin: https://www.solaxcloud.com' -H 'DNT: 1' -H 'Connection: keep-alive' -H 'Sec-Fetch-Dest: empty' -H 'Sec-Fetch-Mode: cors' -H 'Sec-Fetch-Site: same-origin' -H 'Sec-GPC: 1' -H 'Pragma: no-cache' -H 'Cache-Control: no-cache' --data-raw "siteId=${site_id}&time=$d" -o "$OUTFILE"
        sleep $timeout
        d=$(date -I -d "$d + 1 day")
    done
}

# Command line parsing is done here.
argsparse_parse_options "$@"

# Check all parameters and run download_slx_data
if argsparse_is_option_set "longhelp"
then
    show_long_help
    exit 0
else
    if ! argsparse_is_option_set "start_date"
    then
        echo "Start date is mandatory."
        argsparse_usage
        exit 1
    elif ! argsparse_is_option_set "token"
    then
        echo "Security token is mandatory."
        argsparse_usage
        exit 1
    elif ! argsparse_is_option_set "site_id"
    then
        echo "Site ID is mandatory."
        argsparse_usage
        exit 1
    fi
    start_date="${program_options[start_date]}"
    if argsparse_is_option_set "end_date"
    then
        end_date="${program_options[end_date]}"
    else
        end_date=$start_date
    fi
    if ! (valid_date $start_date && valid_date $end_date)
    then
        exit 1
    fi

    if ! smaller_or_equal_date $start_date $end_date
    then
        echo Start date must be before end date.
        exit 1
    fi
    rsleep=${program_options[rsleep]}

    site_id=${program_options[site_id]}
    token=${program_options[token]}

    download_slx_data
fi
