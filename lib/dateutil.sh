#!/bin/bash
#
# Utility functions to work with dates.

# Check if $1 is a date in YYYY-MM-DD format.
# https://stackoverflow.com/a/37144230
# Return 0 if correct format, else 1.
function valid_date(){
    if [[ $1 =~ ^[0-9]{4}-[0-9]{2}-[0-9]{2}$ ]] &&
        date "+%Y-%m-%d" -d "$1" > /dev/null 2>&1
        then
            return 0
        else
            echo $1 is not a date in YYYY-MM-DD format.
            return 1
    fi
}

function smaller_or_equal_date(){
    if [[ "$(date "+%s" -d "$start_date")" -gt "$(date "+%s" -d "$end_date")" ]]
    then
        echo $1 comes after $2.
        return 1
    fi
}
